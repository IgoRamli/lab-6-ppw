from django.shortcuts import render
from django.contrib.auth import authenticate, logout
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def login(request):
    if request.method == 'POST':
        # Authenticate user
        data = request.POST
        user = authenticate(
            username=data['username'],
            password=data['password'])
        if user is not None:
            # Login success
            return render(request, 'login-success.html', {'username': data['username']}, status=200)
        else:
            # Login failed
            return render(request, 'login-fail.html', status=400)
    return render(request, 'login.html', status=200)

def logout_view(request):
    logout(request)
    return render(request, 'logout.html', status=200)
