from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

# Create your tests here.
class LoginTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(username='Admin123',
                                 email='jlennon@beatles.com',
                                 password='admin')
    def test_login_render_success(self):
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 200)

    def test_login_auth_success(self):
        response = self.client.post(reverse("login"), {'username': 'Admin123', 'password': 'admin'})
        self.assertEqual(response.status_code, 200)

    def test_login_auth_fail(self):
        response = self.client.post(reverse("login"), {'username': 'NotAdmin', 'password': 'admin'})
        self.assertEqual(response.status_code, 400)

    def test_logout_auth_success(self):
        # Login first
        response = self.client.post(reverse("login"), {'username': 'Admin123', 'password': 'admin'})
        self.assertEqual(response.status_code, 200)

        # Logout
        response = self.client.post(reverse("logout"))
        self.assertEqual(response.status_code, 200)

    def test_logout_no_login(self):
        # Just log out without any credential
        response = self.client.post(reverse("logout"))
        self.assertEqual(response.status_code, 200)
