function changeTheme() {
	if (!$("body").hasClass("dark-bg")) {
		$("body").addClass("dark-bg");
		$("p").css("color", "white");
		$("b").css("color", "white");
		$("h1").css("color", "white");
		$("label").css("color", "white");
		$("div").css("background-color", "#212121");
	} else {
		$("body").removeClass("dark-bg");
		$("p").css("color", "black");
		$("b").css("color", "black");
		$("h1").css("color", "black");
		$("label").css("color", "black");
		$("div").css("background-color", "white");
	}
}