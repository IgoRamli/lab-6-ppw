from django.test import TestCase
from django.urls import reverse
import time
from selenium import webdriver
from .models import Status

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options

# Create your tests here.

class LandingPageHTMLTest(TestCase):
    def test_landingpage_render_success(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_landingpage_functional(self):
        binary  = FirefoxBinary('/usr/lib/firefox/firefox')
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options = firefox_options, firefox_binary = binary)

        # Opens browser
        self.browser.get('http://localhost:8000/')
        time.sleep(5)

        # Check if all components are present in the page
        greeting_text = self.browser.find_elements_by_tag_name('h1')
        self.assertTrue(len(greeting_text) > 0)
        status_input = self.browser.find_elements_by_xpath('//*[@id="id_status"]')
        self.assertTrue(len(status_input) > 0)
        submit_btn = self.browser.find_elements_by_xpath('/html/body/form/input[3]')
        self.assertTrue(len(submit_btn) > 0)

        # Submit something
        search_box = self.browser.find_element_by_xpath('//*[@id="id_status"]')
        search_box.send_keys('Coba Coba')
        time.sleep(2)
        submit_btn = self.browser.find_element_by_xpath('/html/body/form/input[3]')
        submit_btn.click()
        time.sleep(2)

        # By the time user refreshes, the new data should appear
        self.browser.get('http://localhost:8000/')
        status_list = self.browser.find_elements_by_id('status_list_1')
        self.assertEqual(len(status_list), 1)

        # Bye!
        self.browser.quit()

    def test_landingpage_submit_status_success(self):
        response = self.client.post(reverse('index'), {'status': 'Coba Coba'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Status.objects.all().count(), 1,
            "Status should be accepted")

    def test_landingpage_submit_status_fail(self):
        long_msg = 10*"ThisSentenceIs30CharactersLong" + "!"
        response = self.client.post(reverse('index'), {'status': long_msg})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Status.objects.all().count(), 0,
            "Status with length exceeding 300 characters should not be accepted")
