from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def index(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			form.save()
	form = StatusForm()
	statuses = Status.objects.all()
	return render(request, 'index.html', {'statuses': statuses, 'form': form})